package com.vov4ik.controller;

import com.vov4ik.model.task1.IntMaker;

public class Controller {
    public void showMaxValue(int first, int second, int third) {
        IntMaker intMaker = new IntMaker();
        intMaker.getMaxValue(first, second, third);
    }
    public void showAverageValue(int first, int second, int third) {
        IntMaker intMaker = new IntMaker();
        intMaker.getAverageValue(first, second, third);
    }
}
