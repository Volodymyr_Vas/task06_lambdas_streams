package com.vov4ik.model.arrays;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RandomArray {
    List<Integer> list1;
    List<Integer> list2;
    List<Integer> list3;

    public RandomArray() {
        list1 = Stream.generate(() -> ThreadLocalRandom.current()
                .nextInt(500))
                .limit(10)
                .collect(Collectors.toList());
        list2 = Stream.of(10, 100, 88, 25, 15, 132, 225, 98, 1, 31)
                .collect(Collectors.toList());
        list3 = Stream.iterate(1, n -> n + n*3)
                .limit(10)
                .collect(Collectors.toList());
    }

    double getAverage(List<Integer> intList) {
        IntSummaryStatistics intSummaryStatistics = intList.stream()
                .mapToInt(a -> a)
                .summaryStatistics();
        return intSummaryStatistics.getAverage();
    }

    int getSum(List<Integer> intList) {
        IntSummaryStatistics intSummaryStatistics = intList.stream()
                .mapToInt(a -> a)
                .summaryStatistics();
        return (int) intSummaryStatistics.getSum();
    }

    int getMin(List<Integer> intList) {
        IntSummaryStatistics intSummaryStatistics = intList.stream()
                .mapToInt(a -> a)
                .summaryStatistics();
        return intSummaryStatistics.getMin();
    }

    int getMax(List<Integer> intList) {
        IntSummaryStatistics intSummaryStatistics = intList.stream()
                .mapToInt(a -> a)
                .summaryStatistics();
        return intSummaryStatistics.getMax();
    }
}
