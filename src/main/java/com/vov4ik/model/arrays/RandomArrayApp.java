package com.vov4ik.model.arrays;

import com.vov4ik.view.logger.MyLogger;

public class RandomArrayApp {
    public static void main(String[] args) {
        MyLogger logger = new MyLogger();
        RandomArray randomArray = new RandomArray();
        logger.printInfo("List 1: ");
        randomArray.list1.forEach(System.out::println);
        logger.printInfo("Average of list1: " + randomArray.getAverage(randomArray.list1));
        logger.printInfo("Sum of list1: " + randomArray.getSum(randomArray.list1));
        logger.printInfo("Min of list1: " + randomArray.getMin(randomArray.list1));
        logger.printInfo("Max of list1: " + randomArray.getMax(randomArray.list1));
    }
}
