package com.vov4ik.model.command;

public interface Command {
    void execute(String message);
}
