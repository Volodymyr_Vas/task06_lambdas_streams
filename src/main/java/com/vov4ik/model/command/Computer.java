package com.vov4ik.model.command;

import com.vov4ik.view.logger.MyLogger;

public class Computer {
    private static MyLogger logger = new MyLogger();
    void start(String message) {
        logger.printInfo("Start method: " + message);
    }

    static void stop(String message) {
        logger.printInfo("Stop method: " + message);
    }

    void sleep(String message) {
        logger.printInfo("Sleep method: " + message);
    }

    void restart(String message) {
        logger.printInfo("Restart method: " + message);
    }
}
