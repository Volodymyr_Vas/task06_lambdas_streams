package com.vov4ik.model.command;

public class RestartCommand implements Command {
    Computer computer;

    public RestartCommand(Computer computer) {
        this.computer = computer;
    }

    @Override
    public void execute(String message) {
        Command command = new Command() {
            @Override
            public void execute(String message) {
                computer.restart("Object of command class " + message);
            }
        };
        command.execute(message);
    }
}
