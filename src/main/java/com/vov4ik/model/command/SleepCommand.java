package com.vov4ik.model.command;

public class SleepCommand implements Command {
    Computer computer;

    public SleepCommand(Computer computer) {
        this.computer = computer;
    }

    @Override
    public void execute(String message) {
        new Computer().sleep("With anonymous class " + message);
    }
}
