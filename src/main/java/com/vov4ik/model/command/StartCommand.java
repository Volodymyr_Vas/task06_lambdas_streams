package com.vov4ik.model.command;

public class StartCommand implements Command {
    Computer computer;

    public StartCommand(Computer computer) {
        this.computer = computer;
    }

    @Override
    public void execute(String message) {
        Command lambda = (s) -> computer.start(s);
        lambda.execute("Start with lambda " + message);
    }
}
