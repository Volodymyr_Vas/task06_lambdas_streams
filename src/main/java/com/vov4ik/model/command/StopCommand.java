package com.vov4ik.model.command;

public class StopCommand implements Command {
    Computer computer;

    public StopCommand(Computer computer) {
        this.computer = computer;
    }

    @Override
    public void execute(String message) {
        Command reference = Computer::stop;
        reference.execute("With method reference " + message);
    }
}
