package com.vov4ik.model.command;

public class TestCommand {
    public static void main(String[] args) {
        Computer computer = new Computer();
        User user = new User(
                new StartCommand(computer),
                new StopCommand(computer),
                new SleepCommand(computer),
                new RestartCommand(computer)
        );
        user.startComputer("I'm a FireStarter =)");
        user.restartComputer("I'm a FireRestarter =)");
        user.sleepComputer("I'm a FireSleeper =)");
        user.stopComputer("I'm a FireStopper =)");
    }
}
