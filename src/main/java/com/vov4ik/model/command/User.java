package com.vov4ik.model.command;

public class User {
    Command start;
    Command stop;
    Command sleep;
    Command restart;

    public User(Command start, Command stop, Command sleep, Command restart) {
        this.start = start;
        this.stop = stop;
        this.sleep = sleep;
        this.restart = restart;
    }

    void startComputer(String message) {
        start.execute(message);
    }

    void stopComputer(String message) {
        stop.execute(message);
    }

    void sleepComputer(String message) {
        sleep.execute(message);
    }

    void restartComputer(String message) {
        restart.execute(message);
    }
}
