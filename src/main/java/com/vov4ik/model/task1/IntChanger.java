package com.vov4ik.model.task1;

@FunctionalInterface
interface IntChanger {

    int changeInt(int a, int b, int c);
}
