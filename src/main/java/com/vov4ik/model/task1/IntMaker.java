package com.vov4ik.model.task1;

import com.vov4ik.view.logger.MyLogger;

public class IntMaker {
    private MyLogger logger = new MyLogger();
    public void getMaxValue(int first, int second, int third) {
        IntChanger intChanger = (a, b, c) ->
                (a < b) ? ((b < c) ? c : b) : ((a < c) ? c : a);
        logger.printInfo("Max number is: " + intChanger.changeInt(first, second, third));
    }

    public void getAverageValue(int first, int second, int third) {
        IntChanger intChanger = (a, b, c) -> (a + b + c) / 3;
        logger.printInfo("Average number is: " + intChanger.changeInt(first, second, third));
    }
}
