package com.vov4ik.model.text;

import com.vov4ik.view.logger.MyLogger;

import java.util.*;
import java.util.stream.Collectors;

import static java.lang.Character.isUpperCase;

public class TextLines {
    private List<String> textLinesList = new ArrayList<>();
    private MyLogger logger = new MyLogger();
    private Scanner scanner = new Scanner(System.in);

    void getTextInput() {
        logger.printInfo("Please, enter some lines. Enter an empty line after them to see the result.");
        String textInput;
        do {
            System.out.print("Please, enter next line: ");
            textInput = scanner.nextLine();
            if (!textInput.equals("")) {
                textLinesList.add(textInput);
            }
        } while (!textInput.equals(""));
        logger.printInfo("you have entered:");
        for (String line : textLinesList) {
            logger.printInfo(line);
        }
    }

    void showNumberOfUniqueWords() {
        long numberOfUniqueWords = textLinesList.stream()
                .flatMap(textLinesList -> Arrays.stream(textLinesList.split(" ")))
                .distinct()
                .count();
        logger.printInfo("\nThere are " + numberOfUniqueWords + " unique words in your lines");
    }

    void sortListOfUniqueWords() {
        textLinesList.stream()
                .flatMap(textLinesList -> Arrays.stream(textLinesList.split(" ")))
                .distinct()
                .sorted()
                .forEach((s) -> logger.printInfo(s));
    }

    void showOccurrenceNumberOfEachWord() {
        Map<String, Long> countedWordsMap = textLinesList.stream()
                .flatMap(textLinesList -> Arrays.stream(textLinesList.split(" ")))
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()));
        for (Map.Entry<String, Long> entry : countedWordsMap.entrySet()) {
            logger.printInfo(entry.getKey() + ": " + entry.getValue() + " times in text");
        }
    }

    void showOccurrenceNumberOfEachSymbol() {
        Map<Character, Long> countedCharactersMap = textLinesList.stream()
                .flatMap(textLinesList -> Arrays.stream(textLinesList.split(" ")))
                .flatMapToInt(CharSequence::chars)
                .mapToObj(c -> (char) c)
                .filter(Character::isLowerCase)
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()));
        for (Map.Entry<Character, Long> entry : countedCharactersMap.entrySet()) {
            logger.printInfo(entry.getKey() + ": " + entry.getValue() + " times in text");
        }
    }
}
