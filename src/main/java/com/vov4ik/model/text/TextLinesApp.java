package com.vov4ik.model.text;

public class TextLinesApp {
    public static void main(String[] args) {
        TextLines textLines = new TextLines();
        textLines.getTextInput();
        textLines.showNumberOfUniqueWords();
        textLines.sortListOfUniqueWords();
        textLines.showOccurrenceNumberOfEachWord();
        textLines.showOccurrenceNumberOfEachSymbol();
    }
}
