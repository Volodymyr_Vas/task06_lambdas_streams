package com.vov4ik.view;

import com.vov4ik.controller.Controller;

public class View {
    Controller controller = new Controller();
    public void workWithInt (int first, int second, int third) {
        controller.showMaxValue(first, second, third);
        controller.showAverageValue(first, second, third);
    }
}
